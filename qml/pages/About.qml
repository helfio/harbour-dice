import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: aboutPage
    allowedOrientations: Orientation.Portrait
    forwardNavigation: false
    anchors.fill: parent
    Column{
        id: aboutColumn
        height: parent.height
        width: parent.width
        PageHeader {
            title: qsTr("About Dice")
        }
            Image {
                source:"qrc:///res/logo.png"
//                horizontalAlignment: Image.AlignHCenter
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Rectangle{
                width: parent.width
                height: parent.height/20
                color: "transparent"
            }
            Label{
                width: parent.width
                font.pixelSize: Theme.fontSizeLarge
                font.bold: true
                color: Theme.secondaryHighlightColor
                text: qsTr("Description")
                wrapMode: Text.WordWrap
            }
            Label{
                width: parent.width
                wrapMode: Text.Wrap
                text: qsTr("Dice is an app for when you forget your real dice.")
            }
            Rectangle{
                width: parent.width
                height: parent.height/20
                color: "transparent"
            }
            Label{
                width: parent.width
                font.pixelSize: Theme.fontSizeLarge
                font.bold: true
                color: Theme.secondaryHighlightColor
                text: qsTr("Author")
                wrapMode: Text.WordWrap
            }
            Label{
                width: parent.width
                text:"Juan Pablo Navarro"
            }
            Rectangle{
                width: parent.width
                height: parent.height/20
                color: "transparent"
            }
            Label{
                width: parent.width
                font.pixelSize: Theme.fontSizeLarge
                font.bold: true
                color: Theme.secondaryHighlightColor
                text: qsTr("Github repo")
                wrapMode: Text.WordWrap
            }
            Label{
                width: parent.width
                text:"<a href=\"https://github.com/helfio/harbour-dice\">https://github.com/helfio/harbour-dice</href>"
                wrapMode: Text.WordWrap
            }
            Rectangle{
                width: parent.width
                height: parent.height/15
                color: "transparent"
            }
            Label{
                width: parent.width
                wrapMode: Text.Wrap
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Comments and reports are welcomed!")
            }
    }

}

