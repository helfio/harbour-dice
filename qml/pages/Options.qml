import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.3

Page {
    id: page
    forwardNavigation: false
    Column{
        id: column
        width: page.width
        height: page.height
        PageHeader {
            title: qsTr("Options")
        }
        Row{
            id: row1
            width: parent.width
            height: parent.height/4
            Slider {
                id: diceNumber
                width: parent.width
                height: parent.height
                minimumValue: 1
                maximumValue: 10
                stepSize: 1
                value: 2
                valueText: value.toFixed(2)
                label: "Number of dice"
                onValueChanged: {
                    python.call('something.theRoll.setDiceNumber', [diceNumber.value], function(iDice) {});
                }
            }
        }
        Row{
            id: row2
            width: parent.width
            height: parent.height/4
            Slider {
                id: sides
                width: parent.width
                height: parent.height
                minimumValue: 2
                maximumValue: 20
                stepSize: 1
                value: 6
                valueText: value.toFixed(2)
                label: "Number of sides"
                onValueChanged: {
                    python.call('something.theRoll.setSideNumber', [sides.value], function(iSides) {});
                }
            }
        }
        Row{
            id: row3
            width: parent.width
            height: parent.height/4
        }
        Row{
            id: row4
            width: parent.width
            height: parent.height/4
        }
    }
    Python{
        id: python
//        Load python stuff when anythingelse is already loaded
        Component.onCompleted: {
//            if(modules_unloaded){
//                addImportPath(Qt.resolvedUrl('.')); // Because the python script is in the same folder
//                importModule('something', function () {});
//                modules_unloaded = false;
//            }
            setHandler('iDice',function(diceVar){
                diceNumber.value = diceVar;
            });
            setHandler('iSides',function(sideVar){
                sides.value = sideVar;
            });
            call('something.theRoll.loadDefaults', function() {});
        }

        onReceived:
            {
                // All the stuff you send, not assigned to a 'setHandler', will be shown here:
                console.log('got message from python: ' + data);
            }
    }
}
