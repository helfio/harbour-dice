#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random, pyotherside

class DiceRoll:
    def __init__(self):
        self.side_number = 6
        self.dice_number = 2

    def setSideNumber(self,iSides):
        self.side_number = int(iSides)

    def setDiceNumber(self,iDice):
        self.dice_number = int(iDice)

    def loadDefaults(self):
        pyotherside.send('iDice',self.dice_number)
        pyotherside.send('iSides',self.side_number)
        print("Sending values: "+str(self.dice_number) + " dice and " + str(self.side_number))

    def throwThemAll(self):
        sResult = "Result:\n\n"
        sRoll = []
        iTotal = 0
        for i in range(0,self.dice_number):
            iNumber = random.randint(1,self.side_number)
            iTotal = iTotal + iNumber
            sRoll.append(str(iNumber))
#        sRoll.sort()
#        for singleResult in sRoll:
            
        sOutput = sResult + "+".join(sorted(sRoll)) + " = " + str(iTotal)
        pyotherside.send('result', sOutput)


theRoll = DiceRoll()
