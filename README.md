# harbour-dice

This is a very simple app written in QML+Python for SailfishOS.

It has two purposes:

- Learn how to code apps for SailfishOS.
- Have a dice replacement in the pocket.
